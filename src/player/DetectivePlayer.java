package player;

import scotlandyard.*;
import java.util.*;
import java.util.List;

public class DetectivePlayer {

    private DijkstraAI dijGraph;
    private List<Move> moves;
    private Colour player;
    protected AIGameView aiGameView;

    public DetectivePlayer(AIGameView aiGameView, DijkstraAI dij, List<Move> moves, Colour player) {

        this.aiGameView = aiGameView;
        this.dijGraph = dij;
        this.moves = moves;
        this.player = player;
    }

    public Map<Move, Integer> scoreMoves(){
        System.out.println("------------Begin scoreMoves");
        Map<Move, Integer> moveScores = new HashMap<Move, Integer>();
        int i = 0;
        for (Move move : moves){
            moveScores.put(move, scoreMove(move));
        }
        return moveScores;

    }

    public int scoreMove(Move move){
        System.out.println("------------Begin scoreMove");
        int location; //finding the number of the node you will move to after the full move
        if (move instanceof MoveDouble){
            location = ((MoveDouble) move).move2.target;
        }
        else if (move instanceof MoveTicket){
            location = ((MoveTicket) move).target;
        }
        else{
            location = aiGameView.locatePlayer(player);
        }

        int destination = aiGameView.locatePlayer(Colour.Black);

        Map<Transport, Integer> tickets = new HashMap<Transport, Integer>();
        tickets = aiGameView.getTickets(player);
        List<Integer> list = dijGraph.getRoute(location, destination, tickets);

        System.out.println("------------End scoreMoves and move takes  " + list.size() +
                " steps to reach mr x location.");
        return -list.size();
    }

}
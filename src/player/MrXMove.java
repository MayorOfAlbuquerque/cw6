package player;



import scotlandyard.*;
import swing.algorithms.Dijkstra;

import java.awt.*;
import java.util.*;
import java.util.List;


public class MrXMove {

    private DijkstraAI dijGraph;
    private AIGameView aiGameView;
    List<Move> moves;
    private int defcon;
    private int location;

    public MrXMove(AIGameView aiGameView, DijkstraAI dij, List<Move> moves, int location) {
        this.dijGraph = dij;
        this.aiGameView = aiGameView;
        this.moves = moves;
        this.defcon = setDefcon(location);
        this.location = location;
    }

    private List<Colour> getDetectives() {              //Gets a list of all detectives in the game
        List<Colour> detectives = new ArrayList<>();
        for(Colour player : aiGameView.view.getPlayers()) {
            if (player != Colour.Black) {
                detectives.add(player);
            }
        }
        return detectives;
    }

    private List<Integer> findMoveWithNode(List<Move> moves, int node) {
        List<Integer> nodeList = new ArrayList<Integer>();                                       //Finds moves from a list which go to a specific node
        for(Move move : moves) {
            if(move instanceof MoveTicket) {
                if( ((MoveTicket) move).target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
            if(move instanceof MoveDouble) {
                if( ((MoveDouble) move).move1.target == node || ((MoveDouble) move).move2.target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
        }
        return nodeList;
    }

    private int smallestDistanceToDetectives(int location) {
        int smallestDist = 100;
        for(Colour det : getDetectives()) {
            int dist = aiGameView.locatePlayer(det) - location;
            if(dist < 0) {
                dist = dist*-1;
            }
            if(dist < smallestDist) {
                smallestDist = dist;
            }
        }
        return smallestDist;
    }



    private void setMoveScore(int[] moveScores, List<Integer> listOfIndexes, int valToAdd) {         //Adds value to moves scores
        for (Integer node : listOfIndexes) {
            moveScores[node] = moveScores[node] + valToAdd;
        }
    }

    private int prioritiseTicketType(Ticket ticket, boolean isDouble, boolean defconPriority) {
        if(isDouble && ticket.equals(Ticket.Secret) && defconPriority) {
            return 15;
        }
        else if(!isDouble && ticket.equals(Ticket.Secret) && defconPriority) {
            return 20;
        }
        return 0;
    }

    private int prioritiseHigherDistances(boolean defconPriority, int target) {
        if(target > location + 10 && defconPriority) {
            return 15;
        }
        if(target > location + 20 && defconPriority){
            return 20;
        }
        return 0;
    }

    private int prioritiseMoveType(boolean isDouble, boolean defconPriority) {
        if(isDouble && defconPriority) {
            return 20;
        }
        else if(!isDouble && !defconPriority) {
            return 20;
        }

        return 0;
    }

    private void moveScores(int[] moveScores, boolean defconPriority) {                     //increases array of move scores. Runs through moves to add or remove values depending on
        int index = 0;                                                                      //their usefulness
        for(Move move : moves) {
            if(move instanceof MoveDouble) {
                int moveLocation = ((MoveDouble) move).move2.target;
                int temp = prioritiseMoveType(true, defconPriority);
                temp = temp + prioritiseTicketType(((MoveDouble) move).move1.ticket, true, defconPriority);
                if(((MoveDouble) move).move2.ticket == Ticket.Secret) {
                    temp = temp + prioritiseTicketType(((MoveDouble) move).move2.ticket, true, defconPriority);
                }
                temp = temp + prioritiseHigherDistances(defconPriority, moveLocation);
                if(smallestDistanceToDetectives(moveLocation) < 5) {
                    temp = temp - 10;
                }
                moveScores[index] = temp;
            }
            if( move instanceof MoveTicket) {
                int moveLocation = ((MoveTicket) move).target;
                int temp = prioritiseMoveType(false, defconPriority);
                temp = temp + prioritiseTicketType(((MoveTicket) move).ticket, false, defconPriority);
                temp = temp + prioritiseHigherDistances(defconPriority, moveLocation);
                moveScores[index] = temp;
                if(smallestDistanceToDetectives(moveLocation) < 5) {
                    temp = temp - 10;
                }
            }
            index = index+1;
        }
    }

    private int setDefcon(int location) {
        int defconStatus = 3;
        for(Colour det : getDetectives()){
            if(dijGraph.getRoute(aiGameView.locatePlayer(det), location, aiGameView.getTickets(det)).size() <= 4) {
                if(defconStatus > 2) defconStatus = defconStatus-1;
            }
        }
        if(aiGameView.getRounds().get(aiGameView.getRound())) {
            defconStatus = 1;
            return defconStatus;
        }
        return defconStatus;
    }


    private int getDefcon() {
        return defcon;
    }


    public Map<Move, Integer> scoreMoves() {
        int[] moveScores = new int[moves.size()];                   //Making nodes moving towards detectives negative
        List<Colour> detectives = getDetectives();                   //Move detectives otherwise
        for(int i = 0; i < moveScores.length; i++) {
            moveScores[i] = 0;
        }
        for(Colour det : detectives) {
            List<Integer> quickestRoute = dijGraph.getRoute(aiGameView.locatePlayer(det), location, aiGameView.getTickets(det));
            if (quickestRoute.size() > 2) {
                int nodeToPlayer = quickestRoute.get(quickestRoute.size() - 2);
                if (quickestRoute.size() <= 4) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -15);
                }
                else if (quickestRoute.size() <= 6) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -10);
                }
                else {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -5);
                }
            }
            else {
                int nodeToPlayer = quickestRoute.get(0);
                List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                setMoveScore(moveScores, listOfIndexes, -50);
            }
        }
        if(getDefcon() < 3) {
            moveScores(moveScores, true);
        }
        else {
            moveScores(moveScores, false);
        }

        for(int x = 0; x < moveScores.length; x++) {
            System.out.println(moveScores[x] + "" + moves.get(x));
        }

        Map<Move, Integer> moveMap = new HashMap<Move, Integer>();
        for(int x = 0; x < moves.size(); x++) {
            moveMap.put(moves.get(x), moveScores[x]);
        }

        return moveMap;
    }
}

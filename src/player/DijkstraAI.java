//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package player;

import graph.Edge;
import graph.Graph;
import graph.Node;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import scotlandyard.ScotlandYardGraphReader;
import scotlandyard.Transport;
import swing.algorithms.PageRank;

public class DijkstraAI {
    private PageRank pageRank;
    private Graph<Integer, Transport> graph;
    private List<Node<Integer>> nodes;

    public DijkstraAI(String graphFilename) {
        try {
            ScotlandYardGraphReader e = new ScotlandYardGraphReader();
            this.graph = e.readGraph(graphFilename);
            this.nodes = this.graph.getNodes();
            this.pageRank = new PageRank(this.graph);
            this.pageRank.iterate(100);
        } catch (IOException var3) {
            System.err.println(var3);
        }

    }

    public List<Integer> getRoute(int start, int destination, Map<Transport, Integer> tickets) {
        List nodes = this.graph.getNodes();
        HashMap unvisitedNodes = new HashMap();
        HashMap distances = new HashMap();
        HashMap previousNodes = new HashMap<>();
        Node currentNode = this.graph.getNode(Integer.valueOf(start));
        Node node;
        for(Iterator route = nodes.iterator(); route.hasNext(); previousNodes.put(node, (Object)null)) {
            node = (Node)route.next();
            if(!((Integer)currentNode.getIndex()).equals(node.getIndex())) {
                distances.put(node, Double.valueOf(1.0D / 0.0));
            } else {
                distances.put(node, Double.valueOf(0.0D));
            }

            Integer location = (Integer)node.getIndex();

            try {
                unvisitedNodes.put(node, Double.valueOf(1.0D / this.pageRank.getPageRank(location).doubleValue()));
            } catch (Exception var13) {
                System.err.println(var13);
            }
        }

        while(unvisitedNodes.size() > 0) {
            Node route1 = this.minDistance(distances, unvisitedNodes);
            if(route1 == null) {
                break;
            }

            currentNode = route1;
            if(((Integer)route1.getIndex()).equals(Integer.valueOf(destination))) {
                break;
            }

            unvisitedNodes.remove(route1);
            this.step(this.graph, distances, unvisitedNodes, route1, previousNodes, tickets);
        }

        ArrayList route2;
        for(route2 = new ArrayList(); previousNodes.get(currentNode) != null; currentNode = (Node)previousNodes.get(currentNode)) {
            route2.add(0, currentNode.getIndex());
        }

        route2.add(0, currentNode.getIndex());
        return route2;
    }

    private void step(Graph<Integer, Transport> graph, Map<Node<Integer>, Double> distances, Map<Node<Integer>, Double> unvisitedNodes, Node<Integer> currentNode, Map<Node<Integer>, Node<Integer>> previousNodes, Map<Transport, Integer> tickets) {
        List edges = graph.getEdgesFrom(currentNode);
        Double currentDistance = (Double)distances.get(currentNode);
        Iterator var9 = edges.iterator();

        while(var9.hasNext()) {
            Edge e = (Edge)var9.next();
            Node neighbour = e.getTarget();
            if(unvisitedNodes.get(neighbour) != null) {
                Transport route = (Transport)e.getData();

                Integer numTickets = (Integer)tickets.get(route);
                if(numTickets != null) {
                    Double tentativeDistance = Double.valueOf(currentDistance.doubleValue() + 1.0D / ((double)numTickets.intValue() * this.pageRank.getPageRank((Integer)neighbour.getIndex()).doubleValue()));
                    if(tentativeDistance.doubleValue() < ((Double)distances.get(neighbour)).doubleValue()) {
                        distances.put(neighbour, tentativeDistance);
                        previousNodes.put(neighbour, currentNode);
                    }
                }

            }
        }

    }

    private Node<Integer> minDistance(Map<Node<Integer>, Double> distances, Map<Node<Integer>, Double> unvisitedNodes) {
        Double min = Double.valueOf(1.0D / 0.0);
        Node minNode = null;
        Iterator var5 = distances.entrySet().iterator();

        while(var5.hasNext()) {
            Entry entry = (Entry)var5.next();
            Double d = (Double)entry.getValue();
            if(Double.compare(d.doubleValue(), min.doubleValue()) < 0 && unvisitedNodes.containsKey(entry.getKey())) {
                min = d;
                minNode = (Node)entry.getKey();
            }
        }

        return minNode;
    }
}

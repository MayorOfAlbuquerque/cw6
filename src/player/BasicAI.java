package player;

import scotlandyard.*;
import swing.algorithms.Dijkstra;

import java.util.*;
import java.util.List;
import java.util.Map;

public class BasicAI implements Player {

    protected ScotlandYardView view;
    protected DijkstraAI d;
    protected AIGameView aiGameView;

    public BasicAI(ScotlandYardView view, String graphFilename) {
        this.view = view;
        this.d = new DijkstraAI(graphFilename);
    }

    private Map<Move, Integer> scores(int location, List<Move> moves) {
        Map<Move, Integer> scoreMap = new HashMap<Move, Integer>();

        if(moves.size() == 1) {                     //If only one move in List just return array of 1 element
            scoreMap.put(moves.get(0), 0);
        }

        this.aiGameView = new AIGameView(view);

        if(view.getCurrentPlayer() == Colour.Black) {
            MrXMove xMoves = new MrXMove(aiGameView, d, moves, location);
            scoreMap = xMoves.scoreMoves();                                         //Move MrX if AI is black
        }
        else {
            DetectivePlayer detectivePlayer = new DetectivePlayer(aiGameView, d, moves, view.getCurrentPlayer());
            scoreMap = detectivePlayer.scoreMoves();            //Move MrX if AI is black
        }
        return scoreMap;
    }

    //take in the location of the current player and the list of their valid moves and play their preferred move
    @Override
    public void notify(int location, List<Move> moves, Integer token, Receiver receiver) {

        Map<Move, Integer> scoreMap = scores(location, moves);

        Map.Entry<Move, Integer> maxEntry = null;
        for (Map.Entry<Move, Integer> entry : scoreMap.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        //play the actual move
        Move moveToPlay = maxEntry.getKey();
        receiver.playMove(moveToPlay, token);
    }
}
package player;

import com.sun.org.apache.xerces.internal.impl.dv.xs.BooleanDV;
import com.sun.org.apache.xpath.internal.operations.Bool;
import scotlandyard.*;

import java.awt.*;
import java.awt.print.Book;
import java.util.*;
import java.util.List;
import java.util.concurrent.TransferQueue;


public class AIGameView {

    protected ScotlandYardView view;
    protected Map<Colour, Integer> playerLocations;
    protected Map<Colour, Map<Transport, Integer>> playerTickets;
    protected int round;
    protected List<Boolean> rounds;

    public AIGameView(ScotlandYardView view) {
        this.view = view;
        playerLocations = new HashMap<Colour, Integer>();
        List<Colour> players = view.getPlayers();
        playerTickets = new HashMap<Colour, Map<Transport, Integer>>();
        for(Colour player : players) {
            playerLocations.put(player, view.getPlayerLocation(player));
            playerTickets.put(player, setTickets(player));
        }
        this.round = view.getRound();
        this.rounds = view.getRounds();
    }

    public void movePlayer(Colour player, int newLocation) {
        playerLocations.remove(player);
        playerLocations.put(player, newLocation);
    }

    public int locatePlayer(Colour player) {
        return playerLocations.get(player);
    }


    public Map<Transport, Integer> setTickets (Colour player) {
        Map<Transport, Integer> tickets = new HashMap<Transport, Integer>();
        tickets.put(Transport.Bus, view.getPlayerTickets(player, Ticket.Bus));
        tickets.put(Transport.Taxi, view.getPlayerTickets(player, Ticket.Taxi));
        tickets.put(Transport.Underground, view.getPlayerTickets(player, Ticket.Underground));
        return tickets;
    }

    public Map<Transport, Integer> getTickets(Colour player) {
        return playerTickets.get(player);
    }


    public int getRound() {
        return round;
    }

    public void incRound() {
        round = round+1;
    }
    public List<Boolean> getRounds() {
        return rounds;
    }

}
